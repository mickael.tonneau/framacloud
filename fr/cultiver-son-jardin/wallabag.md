# Installation de wallabag

[Wallabag][1] est une application de lecture différée : une page web
vous intéresse mais vous ne pouvez la lire à l’instant ?
Sauvegardez-là dans votre wallabag pour la lire plus tard (sur navigateur, application mobile, ou liseuse).

Framasoft propose une instance de wallabag nommée bien entendu… [Framabag][2] :-)
Pour une présentation plus complète et en images, n’hésitez pas à jeter
un œil à [notre présentation de wallabag][3].

N’hésitez pas aussi à consulter la [documentation officielle de wallabag][4],
riche en informations !

![](images/wallabag/Wallabag.png)

<div class="alert alert-info">
  <b>Deux installations sont possibles</b> : sur un serveur dédié, ou
  sur un hébergement mutualisé.
  Cependant, l’installation en mutualisé n’est pas celle que
  recommandent les développeurs et elle ne sera pas traitée dans ce
  tutoriel (votre serviteur n’a pas d’hébergement de test compatible).
  Ils fournissent néanmoins
  <a href="http://wllbg.org/latest-v2-package">un paquet contenant toutes les dépendances</a>
  facilitant le déploiement pour ceux qui le souhaitent.
</div>

## Prérequis

La machine utilisée pour ce tutoriel est la suivante :

*   Linux Debian Stretch
*   Être administrateur de la machine ou faire partie du groupe sudo

En option, vous pourriez avoir besoin :

*   d’un nom de domaine correctement configuré pour rediriger vers votre serveur
*   d’un serveur mail opérationnel pour confirmer les nouveaux comptes et la double authentification


## Installation

### 1 - Préparer la terre

![](images/icons/preparer.png)

Ma chère Maïté, pour préparer la recette du wallabag, il nous faut
quelques ustensiles !

Tout d’abord, nous allons avoir besoin de git pour récupérer les sources,
et de curl qui est utilisé en sous-main par wallabag :

    sudo apt install git curl

Sans oublier aussi make, zip et unzip qui seront utilisés lors de l'installation
de wallabag.

    sudo apt install make zip unzip
    
Vous aurez aussi besoin d'un serveur Web pour afficher les pages. Et rien de
tel qu'un serveur rapide et léger pour servir vos pages, du coup, on installe
NginX

    sudo apt install nginx
    
Wallabag utilise aussi PHP pour fonctionner avec moult modules.
Rassurez-vous, la plupart sont fournis par défaut ! Pour ceux qui
manqueraient potentiellement :

    sudo apt-get install php-fpm php-curl php-gd php-tidy php-xml php-mbstring php-bcmath

Pour vérifier que tout ce qu’il faut est installé, un petit `php -m`
vous indiquera quels sont les modules utilisés par PHP (en ligne de
commande – ce n’est pas forcément ce qui sera utilisé par le serveur web,
mais c'est un moyen simple et rapide de voir si le module est installé).

Vous pouvez aussi vérifier que les modules sont activés pour php-fpm,
en vérifiant quels sont les modules activés dans `/etc/php7.0/mods-available/`.
Si vous voyez une ligne du genre `extension=tidy.so` sans point-virgule devant, c’est activé.

Vous devez retrouver ces éléments dans la réponse de `php -m` :

    php-session
    php-ctype
    php-dom
    php-hash
    php-simplexml
    php-json
    php-gd
    php-mbstring
    php-xml
    php-tidy
    php-iconv
    php-curl
    php-gettext
    php-tokenizer
    php-bcmath

Afin de se connecter à une base de données, wallabag utilise un des modules PDO.
Vous aurez donc besoin **d’au moins** une extension et d’un système de
bases de données au choix parmi :

    pdo_mysql
    pdo_sqlite
    pdo_pgsql

Pour la suite, nous utiliseront MariaDB comme serveur de base de données.

Pour cela, on va l'installer lui et son extension PDO.

    sudo apt install mariadb-server php-mysql

Enfin, pour gérer les dépendances PHP et construire l’application,
il nous faudra Composer :

    curl -s http://getcomposer.org/installer | php
    sudo mv composer.phar /bin/composer
    sudo chown root:root /bin/composer


(respectivement : on récupère l'installeur, on copie dans les exécutables
du système et on vérifie que les droits sont bien d'équerre)

### 2 - Semer

![](images/icons/semer.png)

Maintenant que tout est prêt, la météo est bonne, rentrons dans le vif du sujet !

On va commencer à sécuriser la base de données un minimum. Pour cela, on va réaliser
les actions suivantes:
* Mettre un mot de passe pour root
* Désactiver l'accès root depuis l'extérieur
* Supprimer les comptes anonymes
* Supprimer la base test, accessible par les utilisateurs anonymes

Et tout cela, avec la commande:
    
    mysql_secure_installation
    
Une fois lancée, répondez aux questions suivantes:

    Set root password? [Y/n] Y
    Remove anonymous users? [Y/n] Y
    Disallow root login remotely? [Y/n] Y
    Remove test database and access to it? [Y/n] Y
    Reload privilege tables now? [Y/n] Y
    
Une fois la base sécurisée, on va créer la base de données de wallabag et
l'utilisateur associé:

    mysql -u root -p
    CREATE DATABASE wallabag;
    GRANT ALL PRIVILEGES ON wallabag.* TO 'wallabag'@'localhost' IDENTIFIED BY 'un_mot_de_passe';
    FLUSH PRIVILEGES;
    exit;

On se place ensuite dans le répertoire de Nginx (dans notre installation,
`/var/www/html`) et on récupère les sources de wallabag :

    cd /var/www/html
    sudo -u www-data git clone https://github.com/wallabag/wallabag.git

Une fois que tout est téléchargé, on rentre dans le dossier fraîchement
créé, et on lance l'installation dans la pure tradition de Linux :

    cd wallabag
    sudo -u www-data make install

Au lancement de la dernière commande ci-dessus, make va préparer le
package wallabag pour qu’il soit utilisable, et vous posera des questions
sur la configuration à générer.
Laissez les options par défaut si vous ne savez pas quoi mettre.

Quelques recommandations tout de même :

*   dans le cas de SQLite, bien mettre l’utilisateur `database_user`
    comme celui de nginx (ici `www-data`), sinon il lui sera difficile d’y accéder.
*   il est préférable (mais pas indispensable) de renseigner un serveur
    mail fonctionnel, car il peut servir pour créer et vérifier les comptes,
    ainsi que pour la double authentification (`twofactor`).

À titre d’exemple, la configuration utilisée dans le cadre de ce tutoriel :

    Creating the "app/config/parameters.yml" file
    Some parameters are missing. Please provide them.
    database_driver: pdo_mysql
    database_host: 127.0.0.1
    database_port: null
    database_name: wallabag
    database_user: wallabag
    database_password: un_mot_de_passe
    database_path: null
    database_table_prefix: wallabag_
    database_socket: null
    database_charset: utf8mb4
    domain_name: 'https://wallabag.chez-vous.tld'
    mailer_transport: smtp
    mailer_host: smtp.wallabag.chez-vous.tld
    mailer_user: no-reply@wallabag.chez-vous.tld
    mailer_password: GrzUPdlZFuQhkYYyUFkMuw9ZQOSZlNhW
    locale: fr
    secret: 6YDYeAwERsj0hhTND4mOjfOPRrGp9zzQ
    twofactor_auth: false
    twofactor_sender: no-reply@wallabag.chez-vous.tld
    fosuser_registration: true
    fosuser_confirmation: true
    from_email: no-reply@wallabag.chez-vous.tld
    rss_limit: 50
    rabbitmq_host: localhost
    rabbitmq_port: 5672
    rabbitmq_user: guest
    rabbitmq_password: guest
    rabbitmq_prefetch_count: 10
    redis_scheme: tcp
    redis_host: localhost
    redis_port: 6379
    redis_path: null
    redis_password: null

Vous voulez changer un paramètre plus tard ? Tant pis, il faudra tout
réinstaller ! ;-) Blague à part, ces paramètres peuvent se retrouver
dans le dossier d’installation de wallabag, plus précisément dans le
fichier `app/config/parameters.yml`.

Et voila ce que vous devrez avoir :

    Installing Wallabag...

    Step 1 of 5. Checking system requirements.
    +-----------------+--------+----------------+
    | Checked         | Status | Recommendation |
    +-----------------+--------+----------------+
    | PDO Driver      | OK!    |                |
    | curl_exec       | OK!    |                |
    | curl_multi_init | OK!    |                |
    +-----------------+--------+----------------+
    Success! Your system can run Wallabag properly.

    Step 2 of 5. Setting up database.
    Creating database and schema, clearing the cache

    Step 3 of 5. Administration setup.
    Would you like to create a new admin user (recommended) ? (Y/n)Y
    Username (default: wallabag) :
    Password (default: wallabag) : sup3rm0td3p4ssed3r0x0r
    Email:

    Step 4 of 5. Config setup.

    Step 5 of 5. Installing assets.

    Wallabag has been successfully installed.
    Just execute `php bin/console server:run --env=prod` for using wallabag: http://localhost:8000

(n’oubliez pas de renseigner – et mémoriser – le **compte admin** comme
demandé dans l’étape 3)

Enfin pour finaliser et faire bonne mesure, on s’assure que le dossier
`wallabag` fraîchement préparé appartient bien au même utilisateur que Nginx :

    sudo chown -R www-data:www-data /var/www/html/wallabag/

Voilà, wallabag est installé ! Vous pouvez le tester avec un serveur
local en lançant `php bin/console server:run --env=prod` et en allant
voir sur `http://[ip-de-votre-serveur]:8000` (pour tester uniquement !).

### 3 - Arroser

![](images/icons/arroser.png)

La plupart d’entre vous voudront sûrement le mettre derrière un serveur
web (fortement recommandé si ce n’est pas un simple serveur de test !).
Dans notre configuration, il s’agit de Nginx. Les fichiers de configuration
des sites se trouvent dans `/etc/nginx/sites-available`, nous allons
donc créer la configuration à cet endroit :

    cd /etc/nginx/sites-available/
    nano wallabag.mydomain.tld

Reste à remplir ce fichier :

    server {
        server_name domain.tld www.domain.tld;
        root /var/www/html/wallabag/web;
    
        location / {
            # try to serve file directly, fallback to app.php
            try_files $uri /app.php$is_args$args;
        }
        location ~ ^/app\.php(/|$) {
            # Si vous utilisez PHP 5 remplacez
            # /run/php/php7.0 par /var/run/php5
            fastcgi_pass unix:/run/php/php7.0-fpm.sock;
            fastcgi_split_path_info ^(.+\.php)(/.*)$;
            include fastcgi_params;
            # When you are using symlinks to link the document root to the
            # current version of your application, you should pass the real
            # application path instead of the path to the symlink to PHP
            # FPM.
            # Otherwise, PHP's OPcache may not properly detect changes to
            # your PHP files (see https://github.com/zendtech/ZendOptimizerPlus/issues/126
            # for more information).
            fastcgi_param  SCRIPT_FILENAME  $realpath_root$fastcgi_script_name;
            fastcgi_param DOCUMENT_ROOT $realpath_root;
            # Prevents URIs that include the front controller. This will 404:
            # http://domain.tld/app.php/some-path
            # Remove the internal directive to allow URIs like this
            internal;
        }
    
        # return 404 for all other php files not matching the front controller
        # this prevents access to other php files you don't want to be accessible.
        location ~ \.php$ {
            return 404;
        }
    
        error_log /var/log/nginx/wallabag_error.log;
        access_log /var/log/nginx/wallabag_access.log;
    }

<div class="alert alert-info">
  <b>Notes</b> :
  <ul>
    <li>
        Attention ! pour ceux qui ont l’habitude de configurer avec un
        <code>index.html/php</code> à la racine, ici il faut pointer vers le
        sous-dossier <code>web</code> et le fichier <code>app.php</code>.
    </li>
    <li>
        Changez le <code>server_name</code> à votre sauce bien sûr,
        selon les domaines et redirections DNS que vous préférez (nous avons
        mis ici <code>default_server</code> car notre instance de test
        n’avait pas besoin de plus)
    </li>
  </ul>
</div>

Reste à activer cette configuration et redémarrer Nginx pour qu’il la prenne en compte :

    cd /etc/nginx/sites-enabled/
    sudo ln -s ../sites-available/wallabag
    sudo systemctl restart nginx

Plus qu’à aller sur l’adresse souhaitée (à défaut l’adresse IP du serveur)
et vous connecter avec le compte admin précédemment créé.

![](images/wallabag/Wallabag-login.png)

Dans l’état actuel, **si vous n’avez pas renseigné de serveur mail**,
les utilisateurs ne pourront pas créer de compte par eux-mêmes.
Deux solutions possibles : intercepter le mail qui ne s’enverra pas dans
votre mailqueue (`sudo mailq`), ou créer vous-même l’utilisateur via
l’interface de l’admin : `Configuration > Créer un compte`

![](images/wallabag/Wallabag-quickstart.png)

### 4 - Pailler

![](images/icons/pailler.png)

#### Bloquer l’inscription spontanée

Il est désormais possible de bloquer l'inscription de nouveaux utilisateur! Ce
N'était pas le cas dans les versions précédentes.

Pour désactiver les nouveaux utilisateurs, il faudra modifier le paramètre
`fosuser_registration` à `false` au lieu de `true` dans le fichier `app/config/parameters.yml`.

Une fois la modification effectuée, videz le cache de wallabag pour prendre en
compte la modification :

    sudo -u www-data php bin/console cache:clear -e prod
    
#### Mettre en place les traitements asynchrones

Vous avez installé wallabag, il tourne parfaitement mais manque de chance, vous
ou l'un de vos utilisteurs n'arrive pas à importer ses milliers d'articles provenant
de Pocket ?

Vous avez deux solutions. Soit vous désactivez l'import, mais ça, c'est pas très
sportif. Soit vous mettez des tâches asynchrones. Grâce à ces tâches, on va 
pouvoir importer les articles, non pas d'un bloc, mais petit à petit, histoire 
de ne pas saturer le serveur. Et pour cela, rien de tel que RabbitMQ pour 
réaliser tout cela.

Et pour commencer, on va l'installer sur le serveur depuis le dépôt de RabbitMQ
plus à jour que ceux de Debian :

    wget https://www.rabbitmq.com/rabbitmq-signing-key-public.asc
    sudo apt-key add rabbitmq-signing-key-public.asc
    sudo apt-get update
    sudo apt install rabbitmq-server
    
La configuration par défaut de wallabag est capable de se connecter directement
sur RabbitMQ, de ce fait, il n'y a rien à faire. Cependant, si RabbitMQ est déjà
installé et configuré différement, il vous faudra modifier le fichier `app/config/parameters.yml`
en conséquence.

Une fois la configuration effectuée, il est temps de mettre en place les services
systemd.

On va donc créer plusieurs scripts systemd en tant que root, et pour commencer,
celui permettant d'importer ses favoris issus de Chrome.

Pour commencer, créez le script avec `nano /etc/systemd/system/wallabag-chrome.service`.

Et collez le contenu suivant:

    [Unit]
     Description=wallabag-import-chrome
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_chrome -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target

Pour Firefox, `nano /etc/systemd/system/wallabag-firefox.service` :

    [Unit]
     Description=wallabag-import-firefox
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_firefox -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target
     
Pour Instapaper, `nano /etc/systemd/system/wallabag-instapaper.service` :

    [Unit]
     Description=wallabag-import-instapaper
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_instapaper -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target

Pour Pocket, `nano /etc/systemd/system/wallabag-pocket.service` :

    [Unit]
     Description=wallabag-import-pocket
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_pocket -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target
     
Pour readability, `nano /etc/systemd/system/wallabag-pocket.service` :

    [Unit]
     Description=wallabag-import-readability
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_readability -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target

Pour wallabag v1, `nano /etc/systemd/system/wallabag-wallabagv1.service` :

    [Unit]
     Description=wallabag-import-wallabagv1
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_wallabag_v1 -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target

Pour wallabag v2, `nano /etc/systemd/system/wallabag-wallabagv1.service` :

    [Unit]
     Description=wallabag-import-wallabagv2
     After=rabbitmq-server.service
    
    [Service]
     Type=simple
     User=www-data
     WorkingDirectory=/var/www/html/wallabag
     ExecStart=/usr/bin/php bin/console rabbitmq:consumer -e=prod import_wallabag_v2 -w
     TimeoutSec=15
     Restart=always
    
    [Install]
     WantedBy=multi-user.target

Les services étant créées, on va demander à systemd de recharger sa liste de
services.

    systemctl daemon-reload
    
Puis on active les services que l'on vient de créer.

    systemctl enable wallabag-*
    
Et pour finir, on les démarre.

    systemctl start wallabag-*
    
One more thing, les services sont actifs, maintenant, il faut indiquer à wallabag
d'utiliser RabbitMq. Connectez-vous avec l'administrateur wallabag, allez dans
Configuration interne, Import et activez RabbitMQ en mettant 1 dans le champ correspondant.
N'oubliez pas d'appliquer, et vous pourrez enfin réussir vos imports en toute
sérénité.

![](images/wallabag/Wallabag-asynchrone.png)

#### Problème de sessions expirées avec LXC

Si vous utilisez LXC pour gérer vos machines virtuelles, vous risquez d'obtenir
des messages d'erreur dans les logs. C'est un bug[6] référencé chez Debian qui est
toujours ouvert. Pour le contourner, il faut désactiver le service et le lancer
par cron, sans passer par systemd.

Pour désactiver le service :

    systemctl disable phpsessionclean.timer ; systemctl stop phpsessionclean.timer
    
Modifiez ensuite le fichier /etc/cron.d/php

    ##09,39 *     * * *     root   [ -x /usr/lib/php/sessionclean ] && if [ ! -d /run/systemd/system ]; then /usr/lib/php/sessionclean; fi
    09,39 *     * * *     root   [ -x /usr/lib/php/sessionclean ] && /usr/lib/php/sessionclean

#### Mettre à jour wallabag

Il vous suffit de reprendre les dernières sources et de les réinstaller avec `make update`.
Reprenez le chapitre « Semer » et procédez à l’installation, en reprenant
la base de données existante (faites un backup à minima de votre base
de données avant !
Comme on dit de l’autre côté de la Manche, *Better safe than sorry* :-) ).

 [1]: https://www.wallabag.org
 [2]: https://framabag.org/
 [3]: https://framabag.org/cquoi/#/
 [4]: http://doc.wallabag.org/fr/master/user/installation.html
 [5]: https://github.com/wallabag/wallabag/issues/1873
 [6]: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=869182