# Installation de Scrumblr

## Description

[Scrumblr][2] est un logiciel libre distribué sous [licence GPLv3][3]
qui a pour objectif de fournir un tableau de post-it collaboratif en temps réel.
Il s'agit de la solution logicielle qui propulse le service en ligne [Framemo][1].
Du point de vue utilisateur, seul un navigateur Web est nécessaire.
Nous allons dans ce tutoriel voir la manière d'installer la partie serveur.

![](images/scrumblr/framemo.png)

<div class="alert alert-info">
  Ce guide est prévu pour Debian Stable. À l'heure où nous écrivons ces
  mots, il s'agit de Debian 8.5 Jessie.
  Nous partons du principe que vous venez d'installer le système,
  que celui-ci est à jour et que vous avez déjà installé le serveur web
  <a href="http://nginx.org/">Nginx</a>.
</div>

## Nourrir la terre

![](images/icons/preparer.png)

### NodeJS

Scrumblr est une application développée avec le langage de programmation
JavaScript et s'appuie sur la plate-forme [NodeJS][4].
Malheureusement la version de NodeJS disponible dans les dépôts Debian
est assez ancienne. Nous allons installer NodeJS 4 depuis les [dépôts de l'éditeur][5].

    curl -sL https://deb.nodesource.com/setup_4.x > /tmp/install_node_depot.sh

On vérifie le contenu de `/tmp/install_node_depot.sh` pour vérifier que
le script ne fait rien de dangereux (on va le lancer en `root` quand même !),
puis on le lance et on installe NodeJs :

    nano /tmp/install_node_depot.sh
    sudo bash /tmp/install_node_depot.sh
    sudo apt-get install -y nodejs

### Redis

[Redis][6] est le serveur de base de données de Scrumblr.
La version de Debian stable ira bien, il suffit pour l'installer de faire :

    sudo apt-get install redis-server

### Utilisateur dédié

De manière à mieux isoler l'application, nous allons créer un
utilisateur dédié à Scrumblr, du même nom que le logiciel :

    sudo adduser --no-create-home --home /var/www/scrumblr --disabled-login --gecos "Scrumblr" scrumblr

Cette commande crée l'utilisateur *scrumblr* ainsi qu'un groupe du même nom.
Il lui assigne comme répertoire personnel */var/www/scrumblr*,
sans toutefois créer ce répertoire.

## Semer

![](images/icons/semer.png)

### Prérequis

Pour installer Scrumblr, il nous faudra récupérer la dernière version
du code source. Pour cela, il est possible de télécharger
[l'archive correspondante][7] ou d'utiliser *git*.
Nous privilégierons ce dernier, car cela nous permettra de gérer plus
aisément les mises à jour. Pour installer *git*, rien de plus simple :

    sudo apt-get install git

### Scrumblr

Clonons le code source de scrumblr en lançant la commande suivante :

    cd /var/www/
    sudo git clone https://github.com/aliasaria/scrumblr.git
    sudo chown scrumblr: -R /var/www/scrumblr

Ensuite, nous allons lancer l'installation des dépendances :

    cd /var/www/scrumblr
    sudo su scrumblr -s /bin/bash
    npm install
    exit

[Framasky][8] a codé quelques fonctionnalités plutôt pratiques :

*   un système d'import/export
*   le support du [markdown][9]
*   un système de révisions : créeez un point de révision, exportez-le
    et réimportez-le pour y revenir

Pour profiter de ces fonctionnalités (qui ne sont pas encore intégrées
dans le dépôt originel (pull requests [95][10] et [96][11]) :

    cd /var/www/scrumblr
    sudo su scrumblr -s /bin/bash
    git remote add fork https://github.com/ldidry/scrumblr/
    git fetch fork
    git pull fork master

## Arroser

![](images/icons/arroser.png)

### Lancer Scrumblr

Scrumblr est maintenant prêt à être lancé avec `node server.js --port 4242` !
Mais pour ne pas avoir laisser un terminal ouvert, il faut créer un
service **systemd**.
Créez le fichier `/etc/systemd/system/scrumblr.service` :

    [Unit]
    Description=Scrumblr service
    Documentation=https://github.com/aliasaria/scrumblr/
    Requires=network.target
    Requires=redis-server.service
    After=network.target
    After=redis-server.service

    [Service]
    Type=simple
    User=scrumblr
    WorkingDirectory=/var/www/scrumblr
    ExecStart=/usr/bin/node server.js --port 4242

    [Install]
    WantedBy=multi-user.target

Bien sûr, choisissez le port que vous voulez, pourvu qu'il soit
supérieur à 1024 et non utilisé (vérifiez cela avec `sudo lsof -i :4242`)

Ensuite, pour activer et lancer Scrumblr :

    sudo systemctl daemon-reload
    sudo systemctl enable scrumblr.service
    sudo systemctl start scrumblr.service

### Procéder aux mises à jour de Scrumblr

Notre Scrumblr est sorti de terre et se porte bien mais comment le
faire durer ? Voyons cela : utilisons `git` pour récupérer la dernière
version du logiciel :

    cd /var/www/scrumblr
    sudo su scrumblr -s /bin/bash
    git pull
    npm install
    exit
    sudo systemctl restart scrumblr.service

Rien de plus simple !

## Regarder pousser

![](images/icons/pailler.png)

### Mise en place d'un proxy inverse

Scrumblr est capable de fonctionner seul sur un port standard, mais il
est possible que nous ne dédiions pas notre machine à ce service et que
nous ayons d'autres applications Web à lui adjoindre.
Il peut dans ce cas être intéressant d'utiliser un serveur frontal qui
jouera le rôle de proxy inverse vers Scrumblr.
Pour cet exemple, nous choisissons d'employer [Nginx][12] et un domaine
ou sous-domaine dédié à l'instance Scrumblr.

Créons le fichier `/etc/nginx/sites-available/scrumblr`, par exemple
avec `nano` : `sudo nano /etc/nginx/sites-available/scrumblr`.
À l'intérieur, nous optons pour la configuration suivante :

    upstream scrumblr {
        server 127.0.0.1:4242;
    }
    server {
        listen 80;
        listen [::]:80;

        server_name scrumblr.exemple.org;

        access_log  /var/log/nginx/scrumblr.exemple.org.access.log;
        error_log   /var/log/nginx/scrumblr.exemple.org.error.log;

        index index.html;

        root /var/www/html/;

        location / {
            proxy_set_header    Host $host;
            proxy_pass http://framemo;
            proxy_redirect http://scrumblr http://scrumblr.exemple.org;
            proxy_set_header Upgrade $http_upgrade;
            proxy_set_header Connection "upgrade";
        }
    }


Quelques explications :

*   Le port 80 est le port standard pour le Web (protocole HTTP).
    Libre à vous de configurer un accès chiffré ;
*   Le `server_name` est le nom de domaine ou sous-domaine sur lequel
    nous souhaitons que notre instance Scrumblr soit accessible ;
*   Les *_log permettent d'isoler la journalisation des accès et
    éventuelles erreurs de Nginx vers Scrumblr dans des fichiers spécifiques ;
*   Tout le trafic sur ce domaine est redirigé vers l'adresse locale,
    sur le port 4242 (que nous avons choisi dans le service systemd).
*   Enfin, le dernier bloc permet de passer les requêtes vers Scrumblr.

Nous allons ensuite activer cette configuration en créant un lien
symbolique vers la configuration active :

    sudo ln -s /etc/nginx/sites-available/scrumblr /etc/nginx/sites-enabled

Relançons nginx de manière à ce qu'il prenne en compte le nouveau
fichier de configuration :

    sudo nginx -t && sudo nginx -s reload

(`nginx -t` permet de vérifier que la configuration de Nginx est valide
avant de le relancer)

<i class="glyphicon glyphicon-tree-deciduous" aria-hidden="true">

 [1]: https://framemo.org
 [2]: http://scrumblr.ca
 [3]: https://fr.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU
 [4]: http://nodejs.org/
 [5]: https://nodejs.org/en/download/package-manager/#debian-and-ubuntu-based-linux-distributions
 [6]: http://redis.io/
 [7]: https://github.com/aliasaria/scrumblr/archive/master.zip
 [8]: https://luc.frama.io
 [9]: https://fr.wikipedia.org/wiki/Markdown
 [10]: https://github.com/aliasaria/scrumblr/pull/95
 [11]: https://github.com/aliasaria/scrumblr/pull/96
 [12]: http://nginx.org/